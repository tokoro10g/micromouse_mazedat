clear
close all

filename = 'MM2015MMHE.png';
url = ['http://www.ntf.or.jp/mouse/micromouse2015/' filename];

if ~exist(filename, 'file')
    websave(filename,url);
end
if strcmp(filename(end-2:end),'png')
    [IM,~,alpha] = imread(filename);
    alpha = imresize(alpha, [600 nan]);
    BW = alpha < 10;
else
    IM = imread(filename);
    IM = imresize(IM, [600 nan]);
    % adjust this constant if it fails
    BW = rgb2gray(IM)>200;
end
BW = imdilate(BW, ones(3)); BW = imerode(BW, ones(3));

%imshow(BW);pause

%% Locate the bounding box of the maze
BW2 = imfill(~BW,'holes');
BW2 = imerode(BW2, ones(7)); BW2 = imdilate(BW2, ones(7));
%imshow(BW2);pause

s = regionprops(BW2, 'BoundingBox','FilledArea');
[~,idx] = max([s.FilledArea]);
bb = s(idx).BoundingBox;

%% Crop image
M = imcrop(imerode(BW,eye(3)),bb);
imshow(M)

hold on;

%% Determine the number of cells
accum_y = all(imdilate(M,ones(1,32)),2);
ny = nnz(diff(accum_y)==-1);
accum_x = all(imdilate(M,ones(32,1)),1);
nx = nnz(diff(accum_x)==-1);

n = max(ny,nx);
fprintf('assuming the size of maze is %d x %d\n', n, n);
n=32

%% Detect walls for each cell
[h, w] = size(M);
cur_y = round(linspace(3,h-2,n+1));
cur_x = round(linspace(3,w-2,n+1));

[X,Y] = meshgrid(cur_x,cur_y);
plot(X,Y,'*');

mazedat = zeros(n);

for k=1:n
    for l=1:n
        y = round([cur_y(k), (cur_y(k+1)+cur_y(k))/2, cur_y(k+1), (cur_y(k+1)+cur_y(k))/2]);
        x = round([(cur_x(l+1)+cur_x(l))/2, cur_x(l+1), (cur_x(l+1)+cur_x(l))/2, cur_x(l)]);
        data = diag(~M(y,x));
        mazedat(k,l) = [1 2 4 8]*data; % N: 1, E: 2, S: 4, W: 8
    end
end

%% Serialize data
for k=1:n
    fprintf('%x ',mazedat(k,:)); fprintf('\n');
end